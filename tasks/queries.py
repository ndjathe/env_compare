from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.mysql.hooks.mysql import MySqlHook
from datetime import datetime, timedelta
import logging


## Check MySQL connection
def check_mysql_connection(db_conn) -> bool:
    try:
        # Utiliser le hook MySql pour établir une connexion
        hook = MySqlHook(mysql_conn_id=db_conn)
        conn = hook.get_conn()
        cursor = conn.cursor()
        cursor.execute("SELECT 1;")
        result = cursor.fetchone()
        if result:
            logging.info("MySQL database is accessible.")
            return True
        else:
            logging.error("Failed to retrieve data from MySQL database.")
            return False
    except Exception as e:
        logging.error(f"Error connecting to MySQL database: {e}")
        raise
