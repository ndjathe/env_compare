terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.55.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region     = "eu-west-3"
  access_key = var.a_k
  secret_key = var.s_k
}

########## Instance Jenkins

resource "aws_key_pair" "ssh_key" {
  key_name   = "ssh_key"
  public_key = file("./key.pem.pub")
}

resource "aws_instance" "jenkins_server" {
  ami = var.ami_id
  instance_market_options {
    market_type = "spot"
    spot_options {
      max_price = 0.031
    }
  }
  instance_type = "t2.micro"
  key_name      = aws_key_pair.ssh_key.key_name

  # Création d'un groupe de sécurité pour l'instance Jenkins
  vpc_security_group_ids = [aws_security_group.jenkins_sg.id]

  user_data = templatefile("scripts/install_jenkins_server.sh.tpl", {
    aws_access_key = var.a_k,
    aws_secret_key = var.s_k,
    aws_region     = var.region
  })

  tags = {
    Name = "jenkins"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("./key.pem")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = ["while ! test -f /tmp/user_data_complete; do sleep 10; done"]
  }

}

data "aws_ssm_parameter" "jenkins_initial_password" {
  name       = "/jenkins/initialAdminPassword"
  depends_on = [aws_instance.jenkins_server]
}

resource "aws_security_group" "jenkins_sg" {
  name_prefix = "jenkins_sg"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}


