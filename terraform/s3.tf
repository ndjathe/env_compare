######## Bucket S3

resource "aws_s3_bucket" "env_compare" {
  bucket        = var.bucket_name
  force_destroy = true
  depends_on    = [null_resource.lambda_package]
}

resource "aws_s3_bucket_ownership_controls" "bucket_ownership_controls" {
  bucket = aws_s3_bucket.env_compare.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "bucket_acl" {
  depends_on = [aws_s3_bucket_ownership_controls.bucket_ownership_controls]

  bucket = aws_s3_bucket.env_compare.id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "bucket_versioning" {
  bucket = aws_s3_bucket.env_compare.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_object" "dags" {
  bucket = aws_s3_bucket.env_compare.bucket
  key    = "dags/flow.py"
  source = "../dags/flow.py"
}

resource "aws_s3_object" "queries" {
  bucket = aws_s3_bucket.env_compare.bucket
  key    = "queries/default.sql"
  source = "assets/default.sql"
}

#define variables
locals {
  layer_zip_path    = "assets/layer.zip"
  function_zip_path = "scripts/lambda_function.zip"
  function_file     = "terraform/scripts/lambda_function.py"
  layer_name        = "packages_lambda_layer"
  requirements_path = "requirements.txt"
  dir_name          = "python"
  runtime           = "venv"
  venv              = "package-venv"
  is_windows        = true
}

# create zip file from requirements.txt. Triggers only when the file is updated
resource "null_resource" "lambda_package" {
  triggers = {
    requirements = filesha1("../${local.requirements_path}")
  }
  # the command to install python and dependencies to the machine and zips
  provisioner "local-exec" {
    interpreter = local.is_windows ? ["PowerShell", "-Command"] : []
    command     = <<EOT
        # Print message and current directory
        Write-Host "Creating layers with requirements.txt packages..."
        Write-Host (Get-Location)

        # Go to the parent directory
        Set-Location ..

        # Delete the directory if it exists
        if (Test-Path -Path "${local.dir_name}") {
            Remove-Item -Path "${local.dir_name}" -Recurse -Force
        }

        # Delete the zip result if it exists
        if (Test-Path -Path "terraform\${local.function_zip_path}") {
            Remove-Item -Path "terraform\${local.function_zip_path}" -Force
        }

        # Create the directory
        New-Item -Path "${local.dir_name}" -ItemType Directory
        Write-Host "Ready"

        # Create and activate the virtual environment
        #python -m ${local.runtime} ${local.venv}
        #& "${local.venv}\Scripts\Activate.ps1"

        # Install Python dependencies and copy files
        if (Test-Path -Path "${local.requirements_path}") {
            Write-Host "From: requirements.txt file exists..."
            pip install -r ${local.requirements_path} -t ${local.dir_name}
            Copy-Item ${local.function_file} -Destination ${local.dir_name}
            # Compress the directory to a ZIP file
            #Compress-Archive -Path "${local.dir_name}\*" -DestinationPath "terraform\${local.layer_zip_path}"
            Compress-Archive -Path "${local.dir_name}\*" -DestinationPath "terraform\${local.function_zip_path}"
        } else {
            Write-Host "Error: requirements.txt does not exist!"
        }

        # Deactivate the virtual environment
        #& deactivate

        # Delete the Python distribution package modules
        if (Test-Path -Path "${local.dir_name}") {
            Remove-Item -Path "${local.dir_name}" -Recurse -Force
        }

    EOT
  }
}

/*resource "aws_s3_object" "layer" {
  bucket = aws_s3_bucket.env_compare.bucket
  key    = "layer.zip"
  source = "assets/layer.zip"
}*/
