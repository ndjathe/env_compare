#!/bin/bash

# Variables AWS CLI
AWS_ACCESS_KEY_ID="${aws_access_key}"
AWS_SECRET_ACCESS_KEY="${aws_secret_key}"
AWS_DEFAULT_REGION="${aws_region}"

# Mise à jour du système
sudo apt update -y

# Installation de Java
sudo sudo apt install -y openjdk-11-jdk

# Ajout du dépôt Jenkins
sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
                https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
echo deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc] \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null

sudo apt update

# Installation de Jenkins
sudo apt install -y jenkins

# Démarrage de Jenkins
sudo systemctl start jenkins
sudo systemctl enable jenkins

# Installation de Unzip
sudo apt install -y unzip

# Attendre que Jenkins crée le mot de passe initial
sleep 30

# Récupérer le mot de passe initial de Jenkins
JENKINS_PASSWORD=$(sudo cat /var/lib/jenkins/secrets/initialAdminPassword)

# Installer AWS CLI
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install

# Configurer AWS CLI
aws configure set aws_access_key_id $AWS_ACCESS_KEY_ID
aws configure set aws_secret_access_key $AWS_SECRET_ACCESS_KEY
aws configure set default.region $AWS_DEFAULT_REGION

# Stocker le mot de passe dans AWS SSM Parameter Store
aws ssm put-parameter --name "/jenkins/initialAdminPassword" --value "$JENKINS_PASSWORD" --type "String" --overwrite --region eu-west-3

# Créer un fichier de signal de succès
touch /tmp/user_data_complete
