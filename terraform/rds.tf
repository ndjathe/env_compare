######## Instances RDS

## Mots de passe
resource "random_password" "db1_password" {
  length           = 16
  special          = true
  override_special = "#_"
}

resource "random_password" "db2_password" {
  length           = 16
  special          = true
  override_special = "#_"
}


# Security Group
resource "aws_security_group" "rds_sg" {
  name_prefix = "rds_sg_"

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# RDS Instances
resource "aws_db_instance" "db1" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  db_name                = var.db1_name
  username               = var.db1_username
  password               = random_password.db1_password.result
  parameter_group_name   = "default.mysql8.0"
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  db_subnet_group_name   = aws_db_subnet_group.main.name
  skip_final_snapshot    = true
  publicly_accessible    = true
}

resource "aws_db_instance" "db2" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "8.0"
  instance_class         = "db.t3.micro"
  db_name                = var.db2_name
  username               = var.db2_username
  password               = random_password.db2_password.result
  parameter_group_name   = "default.mysql8.0"
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
  db_subnet_group_name   = aws_db_subnet_group.main.name
  skip_final_snapshot    = true
  publicly_accessible    = true
}

# Subnet Group
resource "aws_db_subnet_group" "main" {
  name       = "main"
  subnet_ids = [var.my_subnet1, var.my_subnet2, var.my_subnet3]
}

# Provisioner for db1
resource "null_resource" "init_db1" {
  provisioner "local-exec" {
    command = <<EOT
      mysql -h ${aws_db_instance.db1.address} -u ${var.db1_username} -p${random_password.db1_password.result} < db1.sql
    EOT
  }
  depends_on = [aws_db_instance.db1]
}

# Provisioner for db2
resource "null_resource" "init_db2" {
  provisioner "local-exec" {
    command = <<EOT
      mysql -h ${aws_db_instance.db2.address} -u ${var.db2_username} -p${random_password.db2_password.result} < db2.sql
    EOT
  }

  depends_on = [aws_db_instance.db2]
}