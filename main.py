import os
import zipfile

FILES = [
    {
        'input': 'terraform/scripts/lambda_function.py',
        'output': {'dir': 'terraform/scripts', 'name': 'lambda_function.zip'}
    },
]


def zip_files(files):
    for file in files:
        with zipfile.ZipFile(f'{file["output"]["dir"]}/{file["output"]["name"]}', 'w') as zipf:
            zipf.write(file['input'])


if __name__ == '__main__':
    zip_files(FILES)
