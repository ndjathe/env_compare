##### Instange Jenkins

output "jenkins_url" {
  value = "http://${aws_instance.jenkins_server.public_ip}:8080"
}

output "jenkins_initial_password" {
  value     = data.aws_ssm_parameter.jenkins_initial_password.value
  sensitive = true
}

##### Instances RDS

output "db1_endpoint" {
  value = aws_db_instance.db1.endpoint
}

output "db1_pwd" {
  value     = random_password.db1_password.result
  sensitive = true
}

output "db2_endpoint" {
  value = aws_db_instance.db2.endpoint
}

output "db2_pwd" {
  value     = random_password.db2_password.result
  sensitive = true
}

##### Cluster MWAA

output "airflow_webserver_url" {
  description = "The URL of the Airflow webserver"
  value       = aws_mwaa_environment.airflow.webserver_url
}

##### SNS

output "sns_topic_arn" {
  description = "The ARN of the SNS topic"
  value       = aws_sns_topic.email.arn
}
