resource "aws_ssm_parameter" "db1_name" {
  name  = "/database1/dbname"
  type  = "String"
  value = aws_db_instance.db1.db_name
}

resource "aws_ssm_parameter" "db2_name" {
  name  = "/database2/dbname"
  type  = "String"
  value = aws_db_instance.db2.db_name
}

locals {
  db_endpoint1 = regex("[^:]+", aws_db_instance.db1.endpoint)
  db_endpoint2 = regex("[^:]+", aws_db_instance.db2.endpoint)
}

resource "aws_ssm_parameter" "db1_endpoint" {
  name  = "/database1/endpoint"
  type  = "String"
  value = local.db_endpoint1
}

resource "aws_ssm_parameter" "db2_endpoint" {
  name  = "/database2/endpoint"
  type  = "String"
  value = local.db_endpoint2
}

resource "aws_ssm_parameter" "db1_username" {
  name  = "/database1/username"
  type  = "String"
  value = aws_db_instance.db1.username
}

resource "aws_ssm_parameter" "db2_username" {
  name  = "/database2/username"
  type  = "String"
  value = aws_db_instance.db2.username
}

resource "aws_ssm_parameter" "db1_password" {
  name  = "/database1/password"
  type  = "SecureString"
  value = random_password.db1_password.result
}

resource "aws_ssm_parameter" "db2_password" {
  name  = "/database2/password"
  type  = "SecureString"
  value = random_password.db2_password.result
}
