resource "aws_iam_role" "lambda_role" {
  name = "lambda_rds_s3_role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = "sts:AssumeRole",
        Effect = "Allow",
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "lambda_policy" {
  name = "lambda_rds_s3_policy"
  policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Action = [
          "rds:DescribeDBInstances",
          "rds:Connect",
          "ssm:GetParameter",
          "ssm:GetParameters",
        ],
        Effect   = "Allow",
        Resource = "*"
      },
      {
        "Effect" : "Allow",
        "Action" : "s3:*",
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : "rds:*",
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : "logs:CreateLogGroup",
        "Resource" : "*"
      },
      {
        "Effect" : "Allow",
        "Action" : [
          "logs:CreateLogStream",
          "logs:PutLogEvents"
        ],
        "Resource" : [
          "*"
        ]
      }
    ]
  })
}

resource "aws_iam_role_policy_attachment" "lambda_role_policy_attachment" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = aws_iam_policy.lambda_policy.arn
}

resource "aws_iam_role_policy_attachment" "iam_role_policy_attachment_lambda_vpc_access_execution" {
  role       = aws_iam_role.lambda_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSLambdaVPCAccessExecutionRole"
}

# Security Group pour VPC Endpoints (facultatif, pour contrôler le trafic)
resource "aws_security_group" "vpc_endpoint_sg" {
  name        = "vpc_endpoint_sg"
  description = "Security group for VPC endpoints"
  vpc_id      = var.my_vpc

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["172.31.0.0/16"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# VPC Endpoint pour SSM
resource "aws_vpc_endpoint" "ssm" {
  vpc_id            = var.my_vpc
  service_name      = "com.amazonaws.${var.region}.ssm"
  vpc_endpoint_type = "Interface"
  subnet_ids        = [var.my_subnet1, var.my_subnet2, var.my_subnet3]
  security_group_ids = [aws_security_group.vpc_endpoint_sg.id]

  private_dns_enabled = true
}

# Route tables pour les subnets dans le VPC
data "aws_route_tables" "my_vpc_route_table" {
  vpc_id = var.my_vpc
}

# VPC Endpoint pour S3
resource "aws_vpc_endpoint" "s3" {
  vpc_id            = var.my_vpc
  service_name      = "com.amazonaws.${var.region}.s3"
  vpc_endpoint_type = "Gateway"
  route_table_ids = data.aws_route_tables.my_vpc_route_table.ids

#   private_dns_enabled = true
}

# Security Group
resource "aws_security_group" "lambda_sg" {
  name_prefix = "rds_sg_"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_lambda_function" "etl_function" {
  filename         = "scripts/lambda_function.zip"
  function_name    = "etl-function"
  role             = aws_iam_role.lambda_role.arn
  handler          = "lambda_function.lambda_handler"
  runtime          = "python3.9"
  source_code_hash = filebase64sha256("scripts/lambda_function.zip")
  layers           = ["arn:aws:lambda:eu-west-3:336392948345:layer:AWSSDKPandas-Python39:26"]
  timeout          = 600
  depends_on       = [null_resource.lambda_package]
  vpc_config {
    subnet_ids = [var.my_subnet1, var.my_subnet2, var.my_subnet3]
    security_group_ids = [aws_security_group.lambda_sg.id]
  }

  environment {
    variables = {
      DB1_KEY_PARAM = "database1"
      DB2_KEY_PARAM = "database2"
      BUCKET_NAME   = aws_s3_bucket.env_compare.bucket
      QUERY_FILE    = "queries/default.sql"
      RESULT_S3_DIR = "result"
    }
  }
}

