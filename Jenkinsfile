pipeline {
    agent any

    environment {
        // Remplacez ces variables par les vôtres
        S3_BUCKET = 'envcompare20240624'
        S3_DIRECTORY = 'dags'
        S3_QUERY_DIRECTORY = 'queries'
        GIT_REPOSITORY = 'https://gitlab.com/ndjathe/env_compare.git'
        GIT_BRANCH = 'main'
    }

    stages {
        stage('Checkout') {
            steps {
                git branch: "${env.GIT_BRANCH}", url: "${env.GIT_REPOSITORY}"
            }
        }

        stage('Install Unzip') {
            steps {
                sh '''
                if [ ! -f /usr/bin/unzip ]
                then
                    sudo apt-get update
                    echo "Unzip not found, installing..."
                    sudo apt install -y unzip
                else
                    echo "Unzip is already installed."
                fi
                '''
            }
        }

        stage('Install AWS CLI') {
            steps {
                sh '''
                if [ ! -f /usr/local/bin/aws ]
                then
                    echo "AWS CLI not found, installing..."
                    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
                    unzip awscliv2.zip
                    sudo ./aws/install
                else
                    echo "AWS CLI is already installed."
                fi
                '''
            }
        }

        stage('Copy dags to S3') {
            steps {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'aws']]) {
                    sh '''
                    aws s3 cp dags s3://${S3_BUCKET}/${S3_DIRECTORY}/ --recursive
                    '''
                }
            }
        }

        stage('Copy queries to S3') {
            steps {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'aws']]) {
                    sh '''
                    aws s3 cp queries/* s3://${S3_BUCKET}/${S3_QUERY_DIRECTORY}/
                    '''
                }
            }
        }

        stage('Copy requirements to S3') {
            steps {
                withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: 'aws']]) {
                    sh '''
                    aws s3 cp requirements.txt s3://${S3_BUCKET}/
                    '''
                }
            }
        }
    }

    post {
        always {
            cleanWs()
        }
    }
}
