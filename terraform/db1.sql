CREATE DATABASE IF NOT EXISTS db1;
USE db1;

CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    description VARCHAR(200)
);

INSERT INTO users (name, description) VALUES
('Alice', 'Description A'),
('Bob', 'Description B'),
('Charlie', 'Description C'),
('David', 'Description D'),
('Eve', 'Description E'),
('Frank', 'Description F'),
('Grace', 'Description G'),
('Hannah', 'Description H'),
('Ivy', 'Description I'),
('Jack', 'Description J');