variable "region" {
  description = "Default region"
  type        = string
  default     = "eu-west-3"
}

variable "key_name" {
  description = "Name of the SSH key pair"
  type        = string
}

variable "runtime" {
  description = "Python runtime"
  type        = string
  default     = "python3.9"
}

variable "private_key_path" {
  description = "Path to the private key to access the instance"
  type        = string
}

variable "ami_id" {
  description = "ID of the free tier ubuntu ami selected"
  type        = string
}

variable "jenkins_token_file" {
  description = "Path of the file containing the Jenkins initial token"
  type        = string
  default     = "/var/lib/jenkins/secrets/initialAdminPassword"
}

variable "local_jenkins_token_file" {
  description = "Local path of the file containing the Jenkins initial token"
  type        = string
}

variable "bucket_name" {
  description = "Name of the bucket"
  type        = string
  default     = "envcompare20240624"
}

variable "a_k" {
  type = string
}

variable "s_k" {
  type = string
}

variable "db1_name" {
  description = "The name for the RDS instance 1"
  type        = string
}

variable "db1_username" {
  description = "The username for the RDS instance 1"
  type        = string
}

variable "db1_password" {
  description = "The password for the RDS instance 1"
  type        = string
  sensitive   = true
}

variable "db2_name" {
  description = "The name for the RDS instance 2"
  type        = string
}

variable "db2_username" {
  description = "The username for the RDS instance 2"
  type        = string
}

variable "db2_password" {
  description = "The password for the RDS instance 2"
  type        = string
  sensitive   = true
}

variable "my_vpc" {
  description = "VPC for MWAA"
  type        = string
}

variable "my_subnet1" {
  description = "Subnet ID 1 of RDS instances"
  type        = string
}

variable "my_subnet2" {
  description = "Subnet ID 2 of RDS instances"
  type        = string
}

variable "my_subnet3" {
  description = "Subnet ID 3 of RDS instances"
  type        = string
}

variable "airflow_version" {
  description = "The version of Apache Airflow"
  type        = string
  default     = "2.9.2"
}

variable "max_workers" {
  description = "The maximum number of workers for the Airflow environment"
  type        = number
  default     = 5
}

variable "min_workers" {
  description = "The minimum number of workers for the Airflow environment"
  type        = number
  default     = 2
}

variable "mwaa_role_name" {
  description = "The name of the MWAA execution role"
  type        = string
  default     = "MWAAExecutionRole"
}

variable "email_address" {
  description = "The email address to subscribe to the SNS topic"
  type        = string
}