import json
import boto3
import pymysql
import pandas as pd
from fuzzywuzzy import fuzz
import os
from io import StringIO

def store_to_s3(df, bucket_name, file_name):
    # Convertir le DataFrame en une chaîne CSV
    csv_buffer = StringIO()
    df.to_csv(csv_buffer, index=False)
    # Initialiser un client S3 avec boto3
    s3_client = boto3.client('s3')
    # Uploader le CSV dans S3
    s3_client.put_object(Bucket=bucket_name, Key=file_name, Body=csv_buffer.getvalue())

def lambda_handler(event, context):
    ssm = boto3.client('ssm')
    s3 = boto3.client('s3')

    # Récupérer les paramètres de SSM
    db1_key_param = event.get('DB1_KEY_PARAM')
    db2_key_param = event.get('DB2_KEY_PARAM')
    db1_key_endpoint = f'/{db1_key_param}/endpoint'
    db1_key_db_name = f'/{db1_key_param}/dbname'
    db1_key_username = f'/{db1_key_param}/username'
    db1_key_password = f'/{db1_key_param}/password'
    db2_key_endpoint = f'/{db2_key_param}/endpoint'
    db2_key_db_name = f'/{db2_key_param}/dbname'
    db2_key_username = f'/{db2_key_param}/username'
    db2_key_password = f'/{db2_key_param}/password'
    db1_endpoint = ssm.get_parameter(Name=db1_key_endpoint)['Parameter']['Value']
    db2_endpoint = ssm.get_parameter(Name=db2_key_endpoint)['Parameter']['Value']
    db1_db_name = ssm.get_parameter(Name=db1_key_db_name)['Parameter']['Value']
    db2_db_name = ssm.get_parameter(Name=db2_key_db_name)['Parameter']['Value']
    db1_username = ssm.get_parameter(Name=db1_key_username)['Parameter']['Value']
    db2_username = ssm.get_parameter(Name=db2_key_username)['Parameter']['Value']
    db1_password = ssm.get_parameter(Name=db1_key_password, WithDecryption=True)['Parameter']['Value']
    db2_password = ssm.get_parameter(Name=db2_key_password, WithDecryption=True)['Parameter']['Value']

    # Récupérer la requête SQL depuis S3
    query_bucket = event.get('BUCKET_NAME')
    query_key = event.get('QUERY_FILE')

    query_file = s3.get_object(Bucket=query_bucket, Key=query_key)
    query_sql = query_file['Body'].read().decode('utf-8')

    # Se connecter aux deux bases de données
    connection1 = pymysql.connect(host=db1_endpoint, user=db1_username, password=db1_password, db=db1_db_name)
    connection2 = pymysql.connect(host=db2_endpoint, user=db2_username, password=db2_password, db=db2_db_name)

    data = []
    try:
        with connection1.cursor() as cursor1, connection2.cursor() as cursor2:
            cursor1.execute(query_sql)
            cursor2.execute(query_sql)
            result1 = cursor1.fetchall()
            result2 = cursor2.fetchall()
            field_names1 = [i[0] for i in cursor1.description]
            field_names2 = [i[0] for i in cursor2.description]

            # DataFrames
            df_db1 = pd.DataFrame(result1, columns=field_names1)
            df_db2 = pd.DataFrame(result2, columns=field_names2)

            if len(df_db1) == 0 and len(df_db2) == 0:
                return {
                    'statusCode': 400,
                    'body': json.dumps('No data'),
                    'data': data
                }

            # Compare data
            in_db1_not_db2 = df_db1[~df_db1['name'].isin(df_db2['name'])]
            in_db2_not_db1 = df_db2[~df_db2['name'].isin(df_db1['name'])]

            # Find common names with different descriptions
            common_names = pd.merge(df_db1, df_db2, on='name', suffixes=('_db1', '_db2'))
            different_descriptions = common_names[common_names['description_db1'] != common_names['description_db2']]
            different_descriptions['similarity'] = different_descriptions.apply(
                lambda row: fuzz.ratio(row['description_db1'], row['description_db2']), axis=1
            )

            result_dir = event.get('RESULT_S3_DIR')

            # Save to CSV
            store_to_s3(df=in_db1_not_db2, bucket_name=query_bucket, file_name=f'{result_dir}/in_db1_not_db2.csv')
            store_to_s3(df=in_db2_not_db1, bucket_name=query_bucket, file_name=f'{result_dir}/in_db2_not_db1.csv')
            store_to_s3(df=different_descriptions, bucket_name=query_bucket, file_name=f'{result_dir}/different_descriptions.csv')
            data = [f'{result_dir}/in_db1_not_db2.csv', f'{result_dir}/in_db2_not_db1.csv', f'{result_dir}/different_descriptions.csv']

    except Exception as error:
        return {
            'statusCode': 500,
            'body': json.dumps(str(error)),
            'data': data
        }
    finally:
        connection1.close()
        connection2.close()

    return {
        'statusCode': 200,
        'body': json.dumps('Query processed successfully'),
        'data': data
    }
