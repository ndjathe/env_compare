from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from airflow.utils.dates import days_ago
from airflow.models import Variable
import boto3
import os
import json
import datetime
import zipfile
import io

# Initialisation du DAG
default_args = {
    'owner': 'admin',
    'depends_on_past': False,
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
}

dag = DAG(
    'etl_s3_to_rds',
    default_args=default_args,
    description='ETL DAG to process SQL files from S3, compare data in RDS and store results in S3',
    schedule_interval=None,
    start_date=days_ago(1),
    catchup=False,
)


# Fonctions Python pour les tâches

def check_queries_files(**kwargs):
    s3 = boto3.client('s3')
    bucket_name = Variable.get("bucket", default_var="envcompare20240624")
    prefix = Variable.get("prefix", default_var="queries/")

    response = s3.list_objects_v2(Bucket=bucket_name, Prefix=prefix)
    files = [obj['Key'] for obj in response.get('Contents', []) if obj['Key'].endswith('.sql')]

    kwargs['ti'].xcom_push(key='files', value=files)
    kwargs['ti'].xcom_push(key='num_files', value=len(files))


def check_connection_to_db():
    rds_client = boto3.client('rds')
    db_instances = [Variable.get("db1", default_var="db1"), Variable.get("db2", default_var="db2")]

    for db in db_instances:
        try:
            response = rds_client.describe_db_instances(DBInstanceIdentifier=db)
            status = response['DBInstances'][0]['DBInstanceStatus']
            if status != 'available':
                raise Exception(f"Database {db} is not available. Status: {status}")
        except Exception as e:
            raise Exception(f"Failed to connect to {db}: {str(e)}")


def generate_result_folders(**kwargs):
    s3 = boto3.client('s3')
    bucket_name = Variable.get("bucket", default_var="envcompare20240624")
    files = kwargs['ti'].xcom_pull(key='files')
    prefix = Variable.get("prefix", default_var="queries/")

    result_folders = []
    for file in files:
        folder_name = file.replace(prefix, 'results/').replace('.sql', '')
        s3.put_object(Bucket=bucket_name, Key=(folder_name))
        result_folders.append(folder_name)

    kwargs['ti'].xcom_push(key='result_folders', value=result_folders)


def extract_compare_load(**kwargs):
    lambda_client = boto3.client('lambda')
    files = kwargs['ti'].xcom_pull(key='files')
    result_folders = kwargs['ti'].xcom_pull(key='result_folders')
    lambda_function_name = Variable.get("lambda_function_name", default_var="etl-function")
    db1 = Variable.get("db1_key", default_var="db1")
    db2 = Variable.get("db2_key", default_var="db2")
    bucket_name = Variable.get("bucket", default_var="envcompare20240624")

    responses = []

    for file, folder in zip(files, result_folders):
        response = lambda_client.invoke(
            FunctionName=lambda_function_name,
            #InvocationType='Event',
            InvocationType='RequestResponse',
            Payload=json.dumps({
                'DB1_KEY_PARAM': db1,
                'DB2_KEY_PARAM': db2,
                'BUCKET_NAME': bucket_name,
                'QUERY_FILE': file,
                'RESULT_S3_DIR': folder,
            })
        )

        # Lire la réponse de la Lambda
        response_payload = json.loads(response['Payload'].read().decode('utf-8'))

        # Log de la réponse
        print(f"Lambda response: {response_payload}")

        status = response_payload['statusCode']
        responses.append(response_payload)

        # Gestion des erreurs en fonction du retour de la Lambda
        if status != 200:
            raise Exception(f"Lambda function failed: {response_payload}")

    kwargs['ti'].xcom_push(key='lambdas_result', value=responses)


def notify_admin(**kwargs):
    sns_client = boto3.client('sns')
    s3 = boto3.client('s3')
    bucket_name = Variable.get("bucket", default_var="envcompare20240624")
    sns_topic_arn = Variable.get("sns_topic_arn", default_var="arn:aws:sns:eu-west-3:654654299967:email-topic")
    result_folders = kwargs['ti'].xcom_pull(key='result_folders')

    # Créer un nom de fichier unique avec la date
    timestamp = datetime.datetime.now().strftime("%Y%m%d%H%M%S")
    archive_name = f"archive_{timestamp}.zip"
    archive_key = f"archives/{archive_name}"

    # Create a BytesIO object to store the compressed data
    zip_buffer = io.BytesIO()
    print(f'result folders : {result_folders}')
    for folder in result_folders:
        response = s3.list_objects_v2(Bucket=bucket_name, Prefix=folder)
        for obj in response.get('Contents', []):
            file_key = obj['Key']
            print(file_key)
            if file_key.endswith('.csv'):
                s3_object = s3.get_object(Bucket=bucket_name, Key=obj['Key'])
                # Use the ZipFile module to write the contents of the S3 object to the zip stream
                with zipfile.ZipFile(zip_buffer, 'w') as zip_file:
                    # Write the contents of the S3 object to the zip file
                    zip_file.writestr(obj['Key'], s3_object['Body'].read())

                # Save the zip file to disk
                #with open(f'{folder.rstrip("/")}.zip', 'wb') as f:
                    #f.write(zip_buffer.getvalue())

        # Upload the compressed data to the S3 bucket and delete
        zip_buffer.seek(0)
        s3.put_object(Bucket=bucket_name, Key=archive_key, Body=zip_buffer)
        #os.remove(f'{folder.rstrip("/")}.zip')

        s3_url = s3.generate_presigned_url(
            'get_object',
            Params={'Bucket': bucket_name, 'Key': archive_key},
            ExpiresIn=3600  # Lien valide pendant 1 heure
        )

    sns_client.publish(
        TopicArn=sns_topic_arn,
        Subject='ETL Process Completed',
        Message=f'Results are available at: {s3_url}',
        MessageAttributes={
            'email': {
                'DataType': 'String',
                'StringValue': 'ndjathe.fwnn@gmail.com'
            }
        }
    )

    #for folder in result_folders:
        # Assuming result files are in the folder
    #    response = s3.list_objects_v2(Bucket=bucket_name, Prefix=folder)
    #    for obj in response.get('Contents', []):
    #        file_key = obj['Key']
    #        print(file_key)
    #        if file_key.endswith('.csv'):
    #            file_url = f"https://{bucket_name}.s3.amazonaws.com/{file_key}"
#
    #            sns_client.publish(
    #                TopicArn=sns_topic_arn,
    #                Subject='ETL Process Completed',
    #                Message=f'Results are available at: {file_url}',
    #                MessageAttributes={
    #                    'email': {
    #                        'DataType': 'String',
    #                        'StringValue': 'ndjathe.fwnn@gmail.com'
    #                    }
    #                }
    #            )


def cleanup(**kwargs):
    s3 = boto3.client('s3')
    bucket_name = Variable.get("bucket", default_var="envcompare20240624")
    result_folders = kwargs['ti'].xcom_pull(key='result_folders')

    for folder in result_folders:
        response = s3.list_objects_v2(Bucket=bucket_name, Prefix=folder)
        objects_to_delete = [{'Key': obj['Key']} for obj in response.get('Contents', [])]

        if objects_to_delete:
            print(objects_to_delete)
            s3.delete_objects(Bucket=bucket_name, Delete={'Objects': objects_to_delete})


# Définir les tâches du DAG

check_queries_task = PythonOperator(
    task_id='check_queries_files',
    python_callable=check_queries_files,
    provide_context=True,
    dag=dag,
)

check_db_task = PythonOperator(
    task_id='check_connection_to_db',
    python_callable=check_connection_to_db,
    dag=dag,
)

generate_folders_task = PythonOperator(
    task_id='generate_result_folders',
    python_callable=generate_result_folders,
    provide_context=True,
    dag=dag,
)

etl_task = PythonOperator(
    task_id='extract_compare_load',
    python_callable=extract_compare_load,
    provide_context=True,
    dag=dag,
)

notify_task = PythonOperator(
    task_id='notify_admin',
    python_callable=notify_admin,
    provide_context=True,
    dag=dag,
)

cleanup_task = PythonOperator(
    task_id='cleanup',
    python_callable=cleanup,
    provide_context=True,
    dag=dag,
)

# Définir les dépendances entre les tâches

check_queries_task >> check_db_task >> generate_folders_task >> etl_task >> notify_task >> cleanup_task
