CREATE DATABASE IF NOT EXISTS db2;
USE db2;

CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(50),
    description VARCHAR(200)
);

INSERT INTO users (name, description) VALUES
('Alice', 'Description A'),  -- Identique
('Boby', 'Description B'),    -- Différent
('Charlie', 'Description C'),-- Identique
('David', 'Description D'),  -- Identique
('Eve', 'Description E'),    -- Identique
('Frank', 'Description F'),  -- Identique
('Grace', 'Description G'),  -- Identique
('Hannah', 'Description H'), -- Identique
('Ivy', 'Description X'),    -- Différent
('Jack', 'Description Y');   -- Différent