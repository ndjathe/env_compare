resource "aws_sns_topic" "email" {
  name = "email-topic"
}

resource "aws_sns_topic_subscription" "email" {
  topic_arn = aws_sns_topic.email.arn
  protocol  = "email"
  endpoint  = var.email_address
}
